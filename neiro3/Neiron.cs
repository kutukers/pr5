﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neiro3
{
    class Neiron
    {
		static Random rnd = new Random();
		public const int sizeOfVector = neironInArrayWidth* neironInArrayHeight;
        private double[,] Wi ;
        private double[] Wi2 ;
		public const int neironsCount = 33;
		private double y;
		double [] sum = new double[neironsCount];
		public const int neironInArrayWidth = 10;
		public const int neironInArrayHeight = 10;
		public const int K = 5;

		
		private double error1;
		private double error;

        public double GetRandomNumber(double minimum, double maximum)
		{
			return rnd.NextDouble() * (maximum - minimum) + minimum;
		}

		public Neiron(){ }

		public void SetWiForNeiron(int numNeiron)
		{
			
			Wi = new double[ numNeiron, numNeiron + 1];
			double wo = GetRandomNumber(0, 1);
			for (int i = 0; i < numNeiron; i++)
			{
				for (int j = 0; j < numNeiron; j++)
				{
					Wi[i, j] = new double();
					Wi[i, j] = GetRandomNumber(0, 0);
				}

				Wi[i, numNeiron] = wo;
			}


            Wi2 = new double[sizeOfVector + 1];
            for (int j = 0; j < sizeOfVector; j++)
            {
                Wi2[j] = new double();
                Wi2[j] = GetRandomNumber(0, 0);
            }
            Wi2[sizeOfVector] = wo;

        }
		

    public double OutputSignal1(int numNeiron, int[] xi)
		{
			
				sum[numNeiron] = 0;
				for (int j = 0; j <= sizeOfVector; j++)
				{
					sum[numNeiron] += xi[j] * Wi2[j];
					
				}

			y = 1 / (1 + Math.Pow(Math.E, -sum[numNeiron]));
			return y;
		}

	    public double OutputSignal2(int numNeiron, double[] yi)
		{
			
				sum[numNeiron] = 0;
				for (int j = 0; j <= yi[j]; j++)
				{
					sum[numNeiron] += yi[j] * Wi[numNeiron,j];
				}

			y = 1 / (1 + Math.Pow(Math.E, -sum[numNeiron]));
			return y;
		}

		public double ErrorForNetwork( int[] di, double[] y)
		{
			error = 0;
			for (int i = 0; i < di.Length; i++)
			{
				error += Math.Pow((di[i] - y[i]),2);
			}
				
			return error/2;
		}

		public double ErrorForNeiron( int di, double y)
		{
			error1 = 0;
			error1 = (di - y);
			return error1;
		}

		//коригування вагів для останнього слоя
	    public void ChangeWi(int countOfLayers, int numNeiron, int d, double[,] yi)
	    {
		    double[] B = new double[countOfLayers];
		    //коригування вагів для останнього слоя
		    B[countOfLayers-1] = y * (1 - y) * (d - y);
		    for (int i = 0; i < neironsCount; i++)
		    {
			    Wi[numNeiron, i] = Wi[numNeiron, i] + (0.4 * B[countOfLayers-1] * yi[countOfLayers - 1, i]);
		    }

		    //коригування вагів для вихідного
		    double sumForPrevious;
		   
			    sumForPrevious = 0;
			    for (int i = 0; i <= neironsCount; i++)
			    {
				    sumForPrevious += Wi[1, i] * B[1];
			    }

			    B[1] = y * (1 - y) * sumForPrevious;
			    for (int i = 0; i < neironsCount; i++)
			    {
				    Wi[numNeiron, i] = Wi[numNeiron, i] + (0.4 * B[1] * yi[1,i]);
			    }
		    

		    // Коригування вагів для першого (xi[10,10])
			    sumForPrevious = 0;
			    for (int i = 0; i <= neironsCount; i++)
			    {
				    sumForPrevious += Wi[1, i] * B[1];
			    }

			    B[0] = y * (1 - y) * sumForPrevious;
			    for (int i = 0; i < neironsCount; i++)
			    {
				    Wi2[i] = Wi2[i] + (0.4 * B[0] * yi[0, i]);
			    }
	    }


    }
}
