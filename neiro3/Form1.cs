﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace neiro3
{
	public partial class Form1 : Form
	{
		private static int countOfLeters = 33;
		private static int countOfLayout = 2;
		private Point startP;
		private int[,] XiMatrixForm;
		private int[,] di = new int[countOfLeters, countOfLeters];
		private Neiron neirons = new Neiron();
		private double[] Xj;
		private static int sizeOfVectorXi = Neiron.neironInArrayHeight * Neiron.neironInArrayWidth;
        private double[] Ei;
		private double[] YHide = new double[sizeOfVectorXi];
		double[] YOutput = new double[countOfLeters];
		private int counter;
		private int countOfLesson = 0;


		private string[] alfvet =
		{
			"а", "б", "в", "г", "ґ", "д", "е", "є", "ж", "з", "и", "і", "ї", "й", "к", "л", "м", "н", "о", "п", "р",
			"с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ь", "ю", "я"
		};


		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			neirons = new Neiron();
			neirons.SetWiForNeiron(countOfLeters);

			GraphUtils.ClearImage(pictureBox1);
			dataGridView1.ColumnCount = Neiron.neironInArrayWidth;
			dataGridView1.RowCount = Neiron.neironInArrayHeight;
			dataGridView1.DefaultCellStyle.ForeColor = Color.Green;
		}

		private void UpdateDataGrid(DataGridView grid, int[,] mass)
		{
			for (int n = 0; n < Neiron.neironInArrayWidth; n++)
			{
				DataGridViewColumn column = grid.Columns[n];
				column.Width = 20;
				for (int m = 0; m < Neiron.neironInArrayHeight; m++)
				{
					grid.Rows[m].Cells[n].Value = mass[n, m];
					int color = (int) ((1 - mass[n, m]) * 255);
					grid.Rows[m].Cells[n].Style.BackColor = Color.FromArgb(color, color, color);
				}
			}

			for (int i = 0; i < di.GetLength(0); i++)
			{
				for (int j = 0; j < di.GetLength(1); j++)
				{
					if (i == j)
					{
						di[i, j] = 1;
					}
					else
						di[i, j] = 0;
				}
			}

		}

		private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				Point endP = new Point(e.X, e.Y);
				Bitmap image = (Bitmap) pictureBox1.Image;
				using (Graphics g = Graphics.FromImage(image))
				{
					g.DrawLine(new Pen(Color.Red), startP, endP);
				}

				pictureBox1.Image = image;
				startP = endP;
			}
		}

		private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
		{
			startP = new Point(e.X, e.Y);
		}
		
		private void Verification()
		{
			InitialInpuSignals();
			Ei = new double[countOfLeters];
			YHide = new double[sizeOfVectorXi];
			
			//ініціалізація прихованого шару
				for (int j = 0; j < sizeOfVectorXi; j++)
				{
					YHide[j] = neirons.OutputSignalHide(j, Xj);
				}
			
			// ініціалізація для вихідного шару
				for (int i = 0; i < countOfLeters; i++)
				{
					YOutput[i] = neirons.OutputSignalResult(i, YHide);
				}
				
			

			//вихідний сигнал персептрона
			double max = 0;
			for (int j = 0; j < countOfLeters; j++)
			{
				if (max < YOutput[j])
				{
					max = YOutput[j];
				}
			}

			for (int i = 0; i < countOfLeters; i++)
			{
				if ((YOutput[i] >= 0.9) && (YOutput[i] == max))
				{
					textBox1.Text = $"{alfvet[i]}";
				}
			}
        }

		// перевірити кнопка
		private void button16_Click(object sender, EventArgs e)
		{
			Verification();
		}

		// кнопка не правда 
		private void button17_Click(object sender, EventArgs e)
		{
			IfNotTrue();
		}


		private void IfNotTrue()
		{

			int expectedLitera = -1;
			int[] d = new int[countOfLeters];
			string UserAnswer;

			if (comboBox1.SelectedIndex == -1)
			{
				UserAnswer =
					Microsoft.VisualBasic.Interaction.InputBox("Вчимо нейромережу: введіть правильну букву", "Вчимо",
						$"А");
			}
			else
			{
				UserAnswer = comboBox1.SelectedItem.ToString();
			}

			for (int i = 0; i < countOfLeters; i++)
			{
				if (alfvet[i].ToUpper() == UserAnswer.ToUpper())
				{
					expectedLitera = i;
					break;
				}
			}

			//ініціалізація масиву з очікуваними відповідями
			for (int j = 0; j < countOfLeters; j++)
			{
				d[j] = di[expectedLitera, j];
			}

			for (int k = 0; k < countOfLayout; k++)
			{
				for (int i = 0; i < countOfLeters; i++)
				{
					Ei[i] = neirons.ErrorForNeiron(d[i], YOutput[i]);
                    if (Ei[i] * 100 > 10)
                    {
                        neirons.ChangeWi(i, d[i], YOutput, YHide, Xj);
                    }
                }
			}
		}

		private void InitialInpuSignals()
		{
			int[,] clipArr = GraphUtils.CutImageToArray((Bitmap) pictureBox1.Image,
				new Point(pictureBox1.Width, pictureBox1.Height));
			if (clipArr == null) return;
			XiMatrixForm =
				GraphUtils.LeadArray(clipArr, new int[Neiron.neironInArrayWidth, Neiron.neironInArrayHeight]);
			UpdateDataGrid(dataGridView1, XiMatrixForm);
			counter = 0;
			Xj = new double [sizeOfVectorXi + 1];
			for (int i = 0; i < Neiron.neironInArrayWidth; i++)
			{
				for (int j = 0; j < Neiron.neironInArrayHeight; j++)
				{
					Xj[counter] = XiMatrixForm[i, j];
					counter++;
				}
			}

			Xj[sizeOfVectorXi] = 1;

			pictureBox2.Image = GraphUtils.GetBitmapFromArr(clipArr);
			pictureBox3.Image = GraphUtils.GetBitmapFromArr(XiMatrixForm);
		}

		// очистити форми кнопка
		private void button18_Click(object sender, EventArgs e)
		{
			for (int n = 0; n < Neiron.neironInArrayWidth; n++)
			{
				for (int m = 0; m < Neiron.neironInArrayHeight; m++)
				{
					XiMatrixForm[n, m] = 0;
				}
			}

			UpdateDataGrid(dataGridView1, XiMatrixForm);
			GraphUtils.ClearImage(pictureBox1);
			GraphUtils.ClearImage(pictureBox2);
			GraphUtils.ClearImage(pictureBox3);
		}

		private void button4_Click(object sender, EventArgs e)
		{
			GraphUtils.ClearImage(pictureBox1);
			GraphUtils.ClearImage(pictureBox2);
			GraphUtils.ClearImage(pictureBox3);
			pictureBox1.Image = GraphUtils.DrawLitera(pictureBox1.Image, comboBox1.SelectedItem.ToString());
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		//aвто навчання 
		private void button5_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < Int32.Parse(textBox2.Text); i++)
			{
				comboBox2.Items.Add(i+1);
			}

			for (int i = 0; i < Int32.Parse(textBox2.Text); i++)
			{
				comboBox2.SelectedIndex = i;
				for (int j = 0; j < countOfLeters; j++)
				{
					comboBox1.SelectedIndex = j;

					GraphUtils.ClearImage(pictureBox1);
					GraphUtils.ClearImage(pictureBox2);
					GraphUtils.ClearImage(pictureBox3);
					pictureBox1.Image = GraphUtils.DrawLitera(pictureBox1.Image, comboBox1.SelectedItem.ToString());

					Verification();
					IfNotTrue();
					Verification();
				}

				countOfLesson++;
				label4.Text = countOfLesson.ToString();
			}
			MessageBox.Show("Навчання закінчено");
		}
	}
}

